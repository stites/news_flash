use super::config::AccountConfig;
use super::{FreshRss, GReaderApi};
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{LoginGUI, PasswordLoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, Url, VectorIcon};
use failure::{Fail, ResultExt};
use greader_api::{ApiError as FreshRssError, ApiErrorKind as FreshRssErrorKind, AuthData, GoogleAuth};
use parking_lot::RwLock;
use rust_embed::RustEmbed;
use std::path::PathBuf;
use std::str;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/freshrss/icons"]
struct FreshRssResources;

pub struct FreshRssMetadata;

impl FreshRssMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("freshrss")
    }
}

impl ApiMetadata for FreshRssMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = FreshRssResources::get("feed-service-freshrss.svg").ok_or(FeedApiErrorKind::Resource)?;
        let icon = VectorIcon {
            data: icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = FreshRssResources::get("feed-service-freshrss-symbolic.svg").ok_or(FeedApiErrorKind::Resource)?;
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let login_gui = LoginGUI::Password(PasswordLoginGUI { url: true, http_auth: true });

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("FreshRSS"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            website: match Url::parse("https://freshrss.org") {
                Ok(website) => Some(website),
                Err(_) => None,
            },
            service_type: ServiceType::Remote { self_hosted: true },
            license_type: ServiceLicense::AGPLv3,
            service_price: ServicePrice::Free,
            login_gui,
        })
    }

    fn parse_error(&self, error: &dyn Fail) -> Option<String> {
        if let Some(error) = <dyn Fail>::downcast_ref::<FreshRssError>(error) {
            if let FreshRssErrorKind::GReader(error) = error.kind() {
                return Some(error.errors.join("; "));
            }

            return Some(format!("{}", error));
        }
        None
    }

    fn get_instance(&self, path: &PathBuf, portal: Box<dyn Portal>) -> FeedApiResult<Box<dyn FeedApi>> {
        let account_config = AccountConfig::load(path).context(FeedApiErrorKind::Config)?;
        let mut api: Option<GReaderApi> = None;

        if let Some(url) = account_config.get_url() {
            if let Ok(url) = Url::parse(&url) {
                if let Some(username) = account_config.get_user_name() {
                    if let Some(password) = account_config.get_password() {
                        let url_with_slash = Url::parse(&(url.as_str().to_owned() + "/")).unwrap();
                        api = Some(GReaderApi::new(
                            &url_with_slash,
                            AuthData::Google(GoogleAuth {
                                username,
                                password,
                                auth_token: account_config.get_auth_token(),
                                post_token: account_config.get_post_token(),
                            }),
                        ));
                    }
                }
            }
        }

        let logged_in = api.is_some();

        let freshrss = FreshRss {
            api,
            portal,
            logged_in,
            config: RwLock::new(account_config),
        };
        let template = Box::new(freshrss);
        Ok(template)
    }
}
