use super::config::AccountConfig;
use super::{Fever, FeverApi};
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{LoginGUI, PasswordLoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, Url, VectorIcon};
use failure::{Fail, ResultExt};
use fever_api::{ApiError as FeverError, ApiErrorKind as FeverErrorKind};
use rust_embed::RustEmbed;
use std::path::PathBuf;
use std::str;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/fever/icons"]
struct FeverResources;

pub struct FeverMetadata;

impl FeverMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("fever")
    }
}

impl ApiMetadata for FeverMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = FeverResources::get("feed-service-fever.svg").ok_or(FeedApiErrorKind::Resource).unwrap();
        let icon = VectorIcon {
            data: icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = FeverResources::get("feed-service-fever-symbolic.svg")
            .ok_or(FeedApiErrorKind::Resource)
            .unwrap();
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let login_gui = LoginGUI::Password(PasswordLoginGUI { url: true, http_auth: true });

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("Fever"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            website: match Url::parse("https://feedafever.com/") {
                Ok(website) => Some(website),
                Err(_) => None,
            },
            service_type: ServiceType::Remote { self_hosted: true },
            license_type: ServiceLicense::ApacheV2,
            service_price: ServicePrice::Free,
            login_gui,
        })
    }

    fn parse_error(&self, error: &dyn Fail) -> Option<String> {
        if let Some(error) = <dyn Fail>::downcast_ref::<FeverError>(error) {
            if let FeverErrorKind::Fever(error) = error.kind() {
                return Some(error.error_message);
            }

            return Some(format!("{}", error));
        }
        None
    }

    fn get_instance(&self, path: &PathBuf, portal: Box<dyn Portal>) -> FeedApiResult<Box<dyn FeedApi>> {
        let account_config = AccountConfig::load(path).context(FeedApiErrorKind::Config)?;
        let mut api: Option<FeverApi> = None;

        if let Some(url) = account_config.get_url() {
            if let Ok(url) = Url::parse(&url) {
                if let Some(username) = account_config.get_user_name() {
                    if let Some(password) = account_config.get_password() {
                        api = if let Some(http_user) = account_config.get_http_user_name() {
                            Some(FeverApi::new_with_http_auth(
                                &url,
                                &username,
                                &password,
                                &http_user,
                                account_config.get_http_password().as_deref(),
                            ))
                        } else {
                            Some(FeverApi::new(&url, &username, &password))
                        }
                    }
                }
            }
        }

        let logged_in = api.is_some();

        let fever = Fever {
            api,
            portal,
            logged_in,
            config: account_config,
        };
        let fever = Box::new(fever);
        Ok(fever)
    }
}
