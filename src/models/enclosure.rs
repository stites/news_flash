use crate::models::ArticleID;
use crate::models::Url;
use crate::schema::enclosures;

#[derive(Identifiable, Queryable, PartialEq, Debug, Insertable, Clone)]
#[primary_key(article_id)]
#[table_name = "enclosures"]
pub struct Enclosure {
    pub article_id: ArticleID,
    pub url: Url,
    pub mime_type: Option<String>,
    pub title: Option<String>,
}
