use crate::models::{ArticleID, Url};
use crate::schema::thumbnails;
use chrono::{NaiveDateTime, Utc};
use error::{ThumbnailError, ThumbnailErrorKind};
use failure::ResultExt;
use image::codecs::jpeg::JpegEncoder;
use image::io::Reader;
use image::GenericImageView;
use image::{imageops, ImageEncoder};
use reqwest::{header, Client};
use std::io::Cursor;

const THUMB_DEST_SIZE: u32 = 160;

#[derive(Identifiable, Queryable, Clone, Debug, Insertable, Eq)]
#[primary_key(article_id)]
#[table_name = "thumbnails"]
pub struct Thumbnail {
    pub article_id: ArticleID,
    #[column_name = "timestamp"]
    pub last_try: NaiveDateTime,
    pub format: Option<String>,
    pub etag: Option<String>,
    pub source_url: Option<Url>,
    pub data: Option<Vec<u8>>,
    pub width: Option<i32>,
    pub height: Option<i32>,
}

impl PartialEq for Thumbnail {
    fn eq(&self, other: &Thumbnail) -> bool {
        self.article_id == other.article_id
    }
}

impl Thumbnail {
    pub async fn from_url(url: &str, article_id: &ArticleID, client: &Client) -> Result<Self, ThumbnailError> {
        let head_res = client.head(url).send().await.context(ThumbnailErrorKind::Http)?;
        if let Some(Ok(content_type)) = head_res.headers().get(header::CONTENT_TYPE).map(|hval| hval.to_str()) {
            if !content_type.starts_with("image") {
                log::debug!("Thumnail URL ({}) doesn't point to an image. mime: {}", url, content_type);
                return Err(ThumbnailErrorKind::NotAnImage.into());
            }
        } else {
            log::debug!("No content type header value set for image URL");
            return Err(ThumbnailErrorKind::NotAnImage.into());
        }

        let res = client.get(url).send().await.context(ThumbnailErrorKind::Http)?;

        let etag = res
            .headers()
            .get(reqwest::header::ETAG)
            .map(|hval| hval.to_str().ok())
            .flatten()
            .map(|etag| etag.into());

        let image_data = res.bytes().await.context(ThumbnailErrorKind::Http)?;

        let image = Reader::new(Cursor::new(image_data))
            .with_guessed_format()
            .context(ThumbnailErrorKind::GuessFormat)?
            .decode()
            .context(ThumbnailErrorKind::Decode)?;

        let (original_width, original_height) = image.dimensions();
        let (thumb_width, thumb_height) = Self::calc_thumb_dimensions(original_width, original_height);

        let thumbnail = imageops::thumbnail(&image, thumb_width, thumb_height);
        let (width, height) = thumbnail.dimensions();

        let thumbnail_data = thumbnail.into_vec();

        let mut dest = Cursor::new(Vec::new());
        let encoder = JpegEncoder::new(&mut dest);
        encoder
            .write_image(&thumbnail_data, width, height, image::ColorType::Rgba8)
            .context(ThumbnailErrorKind::Encode)?;

        Ok(Thumbnail {
            article_id: article_id.clone(),
            last_try: Utc::now().naive_utc(),
            format: Some("image/jpeg".into()),
            etag,
            source_url: Some(Url::parse(url).unwrap()),
            data: Some(dest.into_inner()),
            width: Some(width as i32),
            height: Some(height as i32),
        })
    }

    fn calc_thumb_dimensions(original_width: u32, original_height: u32) -> (u32, u32) {
        if original_width <= THUMB_DEST_SIZE && original_height <= THUMB_DEST_SIZE {
            return (original_width, original_height);
        }

        let ratio = (original_width as f64) / (original_height as f64);
        if original_width >= original_height {
            (THUMB_DEST_SIZE, (THUMB_DEST_SIZE as f64 / ratio) as u32)
        } else {
            ((THUMB_DEST_SIZE as f64 * ratio) as u32, THUMB_DEST_SIZE)
        }
    }
}

mod error {
    use failure::{Backtrace, Context, Error, Fail};
    use std::fmt;

    #[derive(Debug)]
    pub struct ThumbnailError {
        inner: Context<ThumbnailErrorKind>,
    }

    #[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
    pub enum ThumbnailErrorKind {
        #[fail(display = "Failed to decode image")]
        Decode,
        #[fail(display = "Failed to guess image format")]
        GuessFormat,
        #[fail(display = "Failed to encode image")]
        Encode,
        #[fail(display = "Http request failed")]
        Http,
        #[fail(display = "Url doesn't point to an image")]
        NotAnImage,
        #[fail(display = "Unknown Error")]
        Unknown,
    }

    impl Fail for ThumbnailError {
        fn cause(&self) -> Option<&dyn Fail> {
            self.inner.cause()
        }

        fn backtrace(&self) -> Option<&Backtrace> {
            self.inner.backtrace()
        }
    }

    impl fmt::Display for ThumbnailError {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            fmt::Display::fmt(&self.inner, f)
        }
    }

    impl From<ThumbnailErrorKind> for ThumbnailError {
        fn from(kind: ThumbnailErrorKind) -> ThumbnailError {
            ThumbnailError { inner: Context::new(kind) }
        }
    }

    impl From<Context<ThumbnailErrorKind>> for ThumbnailError {
        fn from(inner: Context<ThumbnailErrorKind>) -> ThumbnailError {
            ThumbnailError { inner }
        }
    }

    impl From<Error> for ThumbnailError {
        fn from(_: Error) -> ThumbnailError {
            ThumbnailError {
                inner: Context::new(ThumbnailErrorKind::Unknown),
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::Thumbnail;
    use crate::models::ArticleID;
    use reqwest::Client;

    #[tokio::test(flavor = "current_thread")]
    async fn golem_bitcoin() {
        let url = "https://www.golem.de/2102/154364-260020-260017_rc.jpg";
        let _thumb = Thumbnail::from_url(url, &ArticleID::new("asf"), &Client::new()).await.unwrap();
    }

    #[tokio::test(flavor = "current_thread")]
    async fn feaneron() {
        let url = "https://feaneron.files.wordpress.com/2019/05/captura-de-tela-de-2019-05-31-17-48-43.png?w=1200";
        let _thumb = Thumbnail::from_url(url, &ArticleID::new("asf"), &Client::new()).await.unwrap();
    }
}
