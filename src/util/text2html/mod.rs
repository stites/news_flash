use regex::Regex;

pub struct Text2Html;

impl Text2Html {
    pub fn is_html(text: &str) -> bool {
        let check_html = Regex::new(r#"<[a-z][\s\S]*>"#).expect("Failed to create RegEx");
        check_html.is_match(text)
    }

    pub fn process(text: &str) -> String {
        let compress_newlines = Regex::new(r#"[\n]{2,}"#).expect("Failed to create RegEx");
        let text = compress_newlines.replace_all(text, "\n\n");
        text.split("\n\n")
            .map(|s| s.trim())
            .filter(|s| !s.is_empty())
            .map(|s| format!("<p>{}</p>\n", s.replace("\n", "<br>\n")))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::Text2Html;

    #[test]
    pub fn check_html() {
        let html = r#"
<p>Check Out Overview of PBS Terra: https://bit.ly/33otrUn</p>
<p>If you rank the most habitable places in our solar system Venus lands pretty low, with surface temperatures hot enough to melt lead and sulphuric acid rain. And yet it may have just jumped to the front of the pack. In fact, we may have detected the signature of alien life - Venusian life -for the first time.</p>
<p>Sign Up on Patreon to get access to the Space Time Discord! <br>
https://www.patreon.com/pbsspacetime</p>
<p>Check out the Space Time Merch Store<br>
https://pbsspacetime.com/</p>
<p>Sign up for the mailing list to get episode notifications and hear special announcements!<br>
https://tinyurl.com/yx9cusk5</p>
<p>Hosted by Matt O'Dowd<br>
Written by Matt O'Dowd<br>
Graphics by Leonardo Scholzer, Yago Ballarini, & Pedro Osinski<br>
Directed by: Andrew Kornhaber<br>
Camera Operator: Setare Gholipour<br>
Executive Producers: Eric Brown & Andrew Kornhaber</p>
<p>Special Thanks to All our Supporters on Patreon!</p>
<p>Big Bang<br>
Brodie Rao<br>
Scott Gray<br>
Robert Doxtator<br>
Ahmad Jodeh<br>
Caed Aldwych<br>
Radu Negulescu<br>
Alexander Tamas<br>
Morgan Hough<br>
Juan Benet<br>
Fabrice Eap<br>
Mark Rosenthal<br>
David Nicklas</p>
<p>Quasar<br>
Alec S-L<br>
Christina Oegren<br>
Mark Heising<br>
Vinnie Falco</p>
<p>Hypernova<br>
Lucas Morgan<br>
William Bryan<br>
Mark Matthew Bosko<br>
Justin Jermyn<br>
Jason Finn<br>
Антон Кочков<br>
Julian Tyacke<br>
Syed Ansar<br>
John R. Slavik<br>
Mathew<br>
Danton Spivey<br>
Donal Botkin<br>
John Pollock<br>
Edmund Fokschaner<br>
Joseph Salomone<br>
Matthew O'Connor<br>
Chuck Zegar<br>
Jordan Young<br>
Hank S<br>
John Hofmann<br>
Timothy McCulloch</p>
<p>Gamma Ray Burst<br>
Sean Warniaha<br>
Tonyface<br>
John Robinson<br>
A G<br>
Kevin Lee<br>
Adrian Hatch<br>
Yurii Konovaliuk<br>
John Funai<br>
Cass Costello<br>
Geoffrey Short<br>
Bradley Jenkins<br>
Kyle Hofer<br>
Tim Stephani<br>
Luaan<br>
AlecZero<br>
Malte Ubl<br>
Nick Virtue<br>
Scott Gossett<br>
David Bethala<br>
Dan Warren<br>
Patrick Sutton<br>
John Griffith<br>
Daniel Lyons<br>
Josh Thomas<br>
DFaulk<br>
Kevin Warne<br>
Andreas Nautsch<br>
Brandon labonte<br>
Lucas Morgan</p>
        "#;

        let is_html = Text2Html::is_html(html);
        assert!(is_html);
    }

    #[test]
    pub fn youtube() {
        let article = r#"
Check Out Overview of PBS Terra: https://bit.ly/33otrUn



If you rank the most habitable places in our solar system Venus lands pretty low, with surface temperatures hot enough to melt lead and sulphuric acid rain. And yet it may have just jumped to the front of the pack. In fact, we may have detected the signature of alien life - Venusian life -for the first time.



Sign Up on Patreon to get access to the Space Time Discord! 
https://www.patreon.com/pbsspacetime

Check out the Space Time Merch Store
https://pbsspacetime.com/

Sign up for the mailing list to get episode notifications and hear special announcements!
https://tinyurl.com/yx9cusk5

Hosted by Matt O'Dowd
Written by Matt O'Dowd
Graphics by Leonardo Scholzer, Yago Ballarini, & Pedro Osinski
Directed by: Andrew Kornhaber
Camera Operator: Setare Gholipour
Executive Producers: Eric Brown & Andrew Kornhaber

Special Thanks to All our Supporters on Patreon!

Big Bang
Brodie Rao
Scott Gray
Robert Doxtator
Ahmad Jodeh
Caed Aldwych
Radu Negulescu
Alexander Tamas
Morgan Hough
Juan Benet
Fabrice Eap
Mark Rosenthal
David Nicklas

Quasar
Alec S-L
Christina Oegren
Mark Heising
Vinnie Falco

Hypernova
Lucas Morgan
William Bryan
Mark Matthew Bosko
Justin Jermyn
Jason Finn
Антон Кочков
Julian Tyacke
Syed Ansar
John R. Slavik
Mathew
Danton Spivey
Donal Botkin
John Pollock
Edmund Fokschaner
Joseph Salomone
Matthew O'Connor
Chuck Zegar
Jordan Young
Hank S
John Hofmann
Timothy McCulloch

Gamma Ray Burst
Sean Warniaha
Tonyface
John Robinson
A G
Kevin Lee
Adrian Hatch
Yurii Konovaliuk
John Funai
Cass Costello
Geoffrey Short
Bradley Jenkins
Kyle Hofer
Tim Stephani
Luaan
AlecZero
Malte Ubl
Nick Virtue
Scott Gossett
David Bethala
Dan Warren
Patrick Sutton
John Griffith
Daniel Lyons
Josh Thomas
DFaulk
Kevin Warne
Andreas Nautsch
Brandon labonte
Lucas Morgan
"#;

        let html = Text2Html::process(&article);
        let needle = "<p>Check Out Overview of PBS Terra: https://bit.ly/33otrUn</p>";
        assert_eq!(needle, &html[..needle.len()]);
    }
}
